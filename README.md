![preview](preview.png)

# Setup

- Keyboard: Gherkin
- Layout: Custom [Workman](https://workmanlayout.org/)
- PCB: None (hand-wired)
- Keycaps: 26x [Mito's XDA](https://mitormk.com/) + 4x G20
- Switches: Lubed 180g [Tealios](https://zealpc.net/)
- Controller: [Keyplus](https://github.com/ahtn/keyplus)
- Case: [Smashing acrylics](https://smashingacrylics.co.uk/product/gherkin-case-with-stainless-steel-plate/)
- Sleeve: [Specialee made](https://www.specialeemade.com/mk-sleeves/wool-mk-sleeve)
